#!/bin/bash
repo="registry.gitlab.com/gun1x/alpine-sniproxy"
epoch="$(date +%s)"
docker build -t "${repo}:${epoch}" .
docker push "${repo}:${epoch}"
