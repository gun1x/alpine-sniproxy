FROM alpine:latest
RUN apk add --no-cache\
    bash \
    sniproxy \
    iptables

copy start.sh /start.sh
copy sniproxy.conf /etc/sniproxy.conf

ENTRYPOINT ["/bin/bash"]
CMD ["/start.sh"]
