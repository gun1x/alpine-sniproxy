You have to add an env var name `SUBNET_WHITELIST` with the subnets you want to allow, separate by whitespaces.

If that's not ok for you, edit `start.sh`.
