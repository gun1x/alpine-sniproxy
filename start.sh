#!/bin/bash

printHelp() {
  echo '''
    Usage:
    The following environment variables control functionality:
    SERVER_IP (mandatory)
    IPV4 [ true | false ]
  '''
  exit 0
}

if [ -z "$SERVER_IP" ]; then
  printHelp
fi

sed -i "s/SERVER_IP/${SERVER_IP}/" /etc/sniproxy.conf;

if [ "$IPV4" = true ]; then
  sed -i "s/ipv6_only/ipv4_only/" /etc/sniproxy.conf;
fi

sniproxy -f
